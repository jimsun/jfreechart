import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.function.Exp;
import org.apache.commons.math3.analysis.function.Tan;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.chart.renderer.xy.HighLowRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.OHLCDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Part_3_Exponential extends JFrame implements ActionListener {
    private XYDataset dataset;
    private CandlestickRenderer CandleStickRenderer;
    private DateAxis domainAxis;
    private NumberAxis rangeAxis;
    public static XYPlot plot;
    private JFreeChart jfreechart;
    public ChartPanel chartPanel;
    private JPanel panel;
    private XYLineAndShapeRenderer LineRenderer;
    private HighLowRenderer OHLCRenderer;
    private JLabel labelExpression;
    private JLabel labelCoefficients;
    private JButton btnRen;
    private JButton btnUpdate;
    private JTextField newPolynormial;
    private String fractionLegend = "Rational expression";

    // ranges for the x axis
    final int startX = -7;
    final int endX = 5;

    public Part_3_Exponential() {
        setBackground(Color.WHITE);
        getContentPane().setLayout(new BorderLayout(0, 0));

        // labels to dispaly the expression and coefficients
        labelExpression = new JLabel("Expression: ");
        labelCoefficients = new JLabel("Coefficients: ");

        // create the initial dataset to populate the initial plot
        dataset = createDataset();

        jfreechart = ChartFactory.createXYLineChart(
                "Log expression",
                "a value",
                "b value",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        chartPanel = new ChartPanel(jfreechart);
        chartPanel.setMouseZoomable(true);

        panel = new JPanel(new BorderLayout());
        panel.add(chartPanel, BorderLayout.CENTER);
        getContentPane().add(panel);

        jfreechart.setBackgroundPaint(Color.WHITE);
        XYPlot plot = (XYPlot) jfreechart.getPlot();

        // test adding a marker
        //ValueMarker marker = new ValueMarker(1.3);  // position is the value on the axis
        //marker.setPaint(Color.black);
        //plot.addDomainMarker(marker);

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.BLUE);
        renderer.setSeriesPaint(1, Color.GREEN);
        renderer.setSeriesStroke(0, new BasicStroke(1.0f));
        renderer.setSeriesStroke(1, new BasicStroke(3.0f));
        plot.setRenderer(renderer);

        chartPanel.setBackground(Color.WHITE);
        panel.setBackground(Color.WHITE);

        btnRen = new JButton("rendererNUpdate");
        panel.add(btnRen, BorderLayout.NORTH);
        btnRen.addActionListener(this);

        btnUpdate = new JButton("Update graph");
        btnUpdate.setPreferredSize(new Dimension(40, 20));

        // a panel to organize the expression, the coefficients and the button for update
        JPanel expressionPanel = new JPanel();
        expressionPanel.setLayout(new GridLayout(4, 1));

        newPolynormial = new JTextField("3,2");
        expressionPanel.add(labelExpression);
        expressionPanel.add(labelCoefficients);
        expressionPanel.add(newPolynormial);
        expressionPanel.add(btnUpdate);
        btnUpdate.addActionListener(this);

        // include this new expressionPanel in the overal panel
        panel.add(expressionPanel, BorderLayout.SOUTH);

        this.setVisible(true);
        this.setSize(600, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private XYDataset createDataset() {

        final XYSeries _xySeries = new XYSeries(fractionLegend);
        prepareFractionPlot(_xySeries);

        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(_xySeries);

        return dataset;
    }

    @SuppressWarnings("deprecation")
    private JFreeChart createChart(final OHLCDataset dataset) {
        CandleStickRenderer = new CandlestickRenderer();
        CandleStickRenderer.setSeriesStroke(0, new BasicStroke(1.0f,
                BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        CandleStickRenderer.setSeriesPaint(0, Color.black);

        LineRenderer = new XYLineAndShapeRenderer(true, false);
        LineRenderer.setSeriesPaint(0, Color.BLACK);
        LineRenderer.setSeriesPaint(1, Color.BLUE);
        LineRenderer.setSeriesPaint(2, Color.RED);

        OHLCRenderer = new HighLowRenderer();
        OHLCRenderer.setSeriesStroke(0, new BasicStroke(1.0f,
                BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        OHLCRenderer.setSeriesPaint(0, Color.black);

        domainAxis = new DateAxis();
        domainAxis.setAutoRange(true);
        domainAxis.setTickLabelsVisible(true);
        domainAxis.setAutoTickUnitSelection(true);

        rangeAxis = new NumberAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createStandardTickUnits());
        rangeAxis.setAutoRange(true);

        plot = new XYPlot(dataset, domainAxis, rangeAxis, LineRenderer);
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.WHITE);
        plot.setDomainGridlinesVisible(true);
        plot.setRangeGridlinesVisible(true);

        jfreechart = new JFreeChart("Log Expression", new Font(
                "SansSerif", Font.BOLD, 24), plot, false);
        return jfreechart;
    }

    public static void main(String[] args) {
        new Part_3_Exponential();
    }


    @Override
    public void actionPerformed(ActionEvent ae) {
        XYPlot xyplot = (XYPlot) jfreechart.getPlot();
        if (ae.getSource() == btnUpdate) {
            String newData = newPolynormial.getText().trim();
            String[] newAB = newData.split(",");

            double newA = Double.parseDouble(newAB[0]);
            double newB = Double.parseDouble(newAB[1]);

            XYDataset collec = createNewDataset(newA, newB);
            jfreechart.getXYPlot().setDataset(collec);


        } else if (ae.getSource() == btnRen) {
            xyplot.setRenderer(CandleStickRenderer);
            try {
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @SuppressWarnings("deprecation")
    private XYSeriesCollection createNewDataset(double newA, double newB) {
        final XYSeries _xySeries = new XYSeries(fractionLegend);

        prepareFractionPlot(_xySeries);

        final Exp _function = new Exp();
        UnivariateFunction derv = _function.derivative();

        //todo: how to calculate the second derivative for Sin?

        final XYSeries derivative1 = new XYSeries("First Derivative");
        for (int j = startX; j < endX; j++) {
            final double randValue2 = j;
            final double xValue2 = randValue2;
            final double yValue2 = _function.value(xValue2);
            if (yValue2 != Double.NEGATIVE_INFINITY && yValue2 != Double.POSITIVE_INFINITY && yValue2 != Double.NaN) {
                System.out.println("==> Adding xValue2=" + xValue2 + " and yValue2=" + yValue2);
                derivative1.add(xValue2, yValue2);
            } else {
                System.out.println("==> ignore xValue2=" + xValue2 + " and yValue2=" + yValue2);
            }
        }

        final XYSeriesCollection dataset = new XYSeriesCollection();

        dataset.addSeries(_xySeries);
        dataset.addSeries(derivative1);

        double yMax = _xySeries.getMaxY();
        double yMin = _xySeries.getMinY();

        plot = (XYPlot) jfreechart.getPlot();
        ValueMarker marker = new ValueMarker(yMax);  // position is the value on the axis
        marker.setPaint(Color.RED);
        plot.addRangeMarker(marker);

        ValueMarker marker2 = new ValueMarker(yMin);  // position is the value on the axis
        marker2.setPaint(Color.RED);
        plot.addRangeMarker(marker2);

        //todo:
//        Use various colored lines and markers to find points of relative max/min --- 5 points
//        Use various colored lines and markers to find points of inflection --- 4 points
        return dataset;
    }

    private void prepareFractionPlot(XYSeries _xySeries) {

        //for (int i = startX; i < endX; i++) {
        for (int i = -1; i < 10; i++) {
            // could use a random double number, but let's just use the follow loop here
            final double randValue = i;
//            double xValue = rng.sample();
            double xValue = i;
            double yValue = Math.exp(xValue);
            System.out.println("Adding x=" + randValue + " and y=" + yValue);

            try {
                if (yValue != Double.NEGATIVE_INFINITY && yValue != Double.POSITIVE_INFINITY) {
                    _xySeries.add(xValue, yValue);
                } else {
                    System.out.println("Ignore yValue=" + yValue);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                _xySeries.add(xValue, Double.NaN);
            }
        }

        System.out.println("=========================");
    }
}
