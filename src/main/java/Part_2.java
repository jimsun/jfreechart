import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.chart.renderer.xy.HighLowRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.OHLCDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Part_2 extends JFrame implements ActionListener {
    private XYDataset dataset;
    private CandlestickRenderer CandleStickRenderer;
    private DateAxis domainAxis;
    private NumberAxis rangeAxis;
    public static XYPlot plot;
    private JFreeChart jfreechart;
    public ChartPanel chartPanel;
    private JPanel panel;
    private XYLineAndShapeRenderer LineRenderer;
    private HighLowRenderer OHLCRenderer;
    private JLabel labelExpression;
    private JLabel labelCoefficients;
    private JButton btnRen;
    private JButton btnUpdate;
    private JTextField newPolynormial;
    private String fractionLegend = "Rational expression";

    // ranges for the x axis
    final int startX = -7;
    final int endX = 5;

    public Part_2() {
        setBackground(Color.WHITE);
        getContentPane().setLayout(new BorderLayout(0, 0));

        // labels to dispaly the expression and coefficients
        labelExpression = new JLabel("Expression: ");
        labelCoefficients = new JLabel("Coefficients: ");

        // create the initial dataset to populate the initial plot
        dataset = createDataset();

        jfreechart = ChartFactory.createXYLineChart(
                "Polynormial polynomial rational expression",
                "a value",
                "b value",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        chartPanel = new ChartPanel(jfreechart);
        chartPanel.setMouseZoomable(true);

        panel = new JPanel(new BorderLayout());
        panel.add(chartPanel, BorderLayout.CENTER);
        getContentPane().add(panel);

        jfreechart.setBackgroundPaint(Color.WHITE);
        XYPlot plot = (XYPlot) jfreechart.getPlot();

        // test adding a marker
        //ValueMarker marker = new ValueMarker(1.3);  // position is the value on the axis
        //marker.setPaint(Color.black);
        //plot.addDomainMarker(marker);

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.RED);
        renderer.setSeriesPaint(1, Color.GREEN);
        renderer.setSeriesStroke(0, new BasicStroke(4.0f));
        renderer.setSeriesStroke(1, new BasicStroke(3.0f));
        plot.setRenderer(renderer);

        chartPanel.setBackground(Color.WHITE);
        panel.setBackground(Color.WHITE);

        btnRen = new JButton("rendererNUpdate");
        panel.add(btnRen, BorderLayout.NORTH);
        btnRen.addActionListener(this);

        btnUpdate = new JButton("Update graph");
        btnUpdate.setPreferredSize(new Dimension(40, 20));

        // a panel to organize the expression, the coefficients and the button for update
        JPanel expressionPanel = new JPanel();
        expressionPanel.setLayout(new GridLayout(4, 1));

        newPolynormial = new JTextField("3,2");
        expressionPanel.add(labelExpression);
        expressionPanel.add(labelCoefficients);
        expressionPanel.add(newPolynormial);
        expressionPanel.add(btnUpdate);
        btnUpdate.addActionListener(this);

        // include this new expressionPanel in the overal panel
        panel.add(expressionPanel, BorderLayout.SOUTH);

        this.setVisible(true);
        this.setSize(600, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private XYDataset createDataset() {

        final XYSeries fraction = new XYSeries(fractionLegend);
        prepareFractionPlot(3, 2, fraction);

        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(fraction);

        return dataset;
    }

    @SuppressWarnings("deprecation")
    private JFreeChart createChart(final OHLCDataset dataset) {
        CandleStickRenderer = new CandlestickRenderer();
        CandleStickRenderer.setSeriesStroke(0, new BasicStroke(1.0f,
                BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        CandleStickRenderer.setSeriesPaint(0, Color.black);

        LineRenderer = new XYLineAndShapeRenderer(true, false);
        LineRenderer.setSeriesPaint(0, Color.BLACK);
        LineRenderer.setSeriesPaint(1, Color.BLUE);
        LineRenderer.setSeriesPaint(2, Color.RED);

        OHLCRenderer = new HighLowRenderer();
        OHLCRenderer.setSeriesStroke(0, new BasicStroke(1.0f,
                BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        OHLCRenderer.setSeriesPaint(0, Color.black);

        domainAxis = new DateAxis();
        domainAxis.setAutoRange(true);
        domainAxis.setTickLabelsVisible(true);
        domainAxis.setAutoTickUnitSelection(true);

        rangeAxis = new NumberAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createStandardTickUnits());
        rangeAxis.setAutoRange(true);

        plot = new XYPlot(dataset, domainAxis, rangeAxis, LineRenderer);
        plot.setBackgroundPaint(Color.LIGHT_GRAY);
        plot.setDomainGridlinePaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.WHITE);
        plot.setDomainGridlinesVisible(true);
        plot.setRangeGridlinesVisible(true);

        jfreechart = new JFreeChart("Polynormial Retional Expression", new Font(
                "SansSerif", Font.BOLD, 24), plot, false);
        return jfreechart;
    }

    public static void main(String[] args) {
        new Part_2();
    }


    @Override
    public void actionPerformed(ActionEvent ae) {
        XYPlot xyplot = (XYPlot) jfreechart.getPlot();
        if (ae.getSource() == btnUpdate) {
            String newData = newPolynormial.getText().trim();
            String[] newAB = newData.split(",");

            double newA = Double.parseDouble(newAB[0]);
            double newB = Double.parseDouble(newAB[1]);

            XYDataset collec = createNewDataset(newA, newB);
            jfreechart.getXYPlot().setDataset(collec);


        } else if (ae.getSource() == btnRen) {
            xyplot.setRenderer(CandleStickRenderer);
            try {
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @SuppressWarnings("deprecation")
    private XYSeriesCollection createNewDataset(double newA, double newB) {
        final XYSeries fraction = new XYSeries(fractionLegend);

//        final RealDistribution rng = new UniformRealDistribution(-100, 100);
//        rng.reseedRandomGenerator(64925784252L);

        final PolynomialFunction f = prepareFractionPlot(newA, newB, fraction);

        UnivariateFunction derv = f.derivative();
        final XYSeries polyDerivative1 = new XYSeries("First Derivative");
        for (int j = startX; j < endX; j++) {
            final double randValue2 = j;
            final double xValue2 = randValue2;
            final double yValue2 = derv.value(xValue2);
            polyDerivative1.add(xValue2, yValue2);
        }

        UnivariateFunction derv2 = f.polynomialDerivative().derivative();
        final XYSeries polyDerivative2 = new XYSeries("Second Derivative");
        for (int j = startX; j < endX; j++) {
            final double randValue2 = j;
            final double xValue2 = randValue2;
            final double yValue2 = derv2.value(xValue2);
            polyDerivative2.add(xValue2, yValue2);
        }

        final XYSeriesCollection dataset = new XYSeriesCollection();

        dataset.addSeries(fraction);
        dataset.addSeries(polyDerivative1);
        dataset.addSeries(polyDerivative2);

        plot = (XYPlot) jfreechart.getPlot();
        ValueMarker marker = new ValueMarker(1.3);  // position is the value on the axis
        marker.setPaint(Color.black);
        plot.addDomainMarker(marker);

        //todo:
//        Use various colored lines and markers to find points of relative max/min --- 5 points
//        Use various colored lines and markers to find points of inflection --- 4 points
        return dataset;
    }

    /**
     * This will use a polynorminal function like this:
     *     (x + a)* (x-b) / (x+a)
     * @param newA
     * @param newB
     * @param fraction
     * @return
     */
    private PolynomialFunction prepareFractionPlot(double newA, double newB, XYSeries fraction) {
        final double[] coeff = {newA, newB};

        System.out.println("Fraction expression: " + "(x+" + newA + ")*(x-" + newB + ")/(x+" + newA + ")");
        labelExpression.setText("Expression: " + "(x+a)*(x-b)/(x+a)");
        labelCoefficients.setText("Coefficients: " + "a=" + newA + " and b=" + newB);

        final PolynomialFunction f = new PolynomialFunction(coeff);

        for (int i = startX; i < endX; i++) {
            // could use a random double number, but let's just use the follow loop here
            final double randValue = i;
            double xValue = (randValue + newA) * (randValue - newB);
            double yValue = (randValue + newA);
            try {
                final Fraction f2 = new Fraction((int) xValue, (int) yValue);
                fraction.add(xValue, f2.getFractionDecimal());
            } catch (Exception e) {
                System.out.println(e.getMessage());
                fraction.add(xValue, Double.NaN);
            }
        }
        return f;
    }
}
