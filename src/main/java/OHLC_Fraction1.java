import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.chart.renderer.xy.HighLowRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Month;
import org.jfree.data.time.ohlc.OHLCSeries;
import org.jfree.data.xy.OHLCDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


//http://www.jfree.org/forum/viewtopic.php?t=37977

public class OHLC_Fraction1 extends JFrame implements ActionListener {
    private static final long serialVersionUID = 1L;

    private XYDataset dataset;
    private CandlestickRenderer CandleStickRenderer;
    private DateAxis domainAxis;
    private NumberAxis rangeAxis;
    public static XYPlot plot;
    private JFreeChart jfreechart;
    public ChartPanel chartPanel;
    private JPanel panel;
    private XYLineAndShapeRenderer LineRenderer;
    private HighLowRenderer OHLCRenderer;
    private JLabel labelExpression;
    private JLabel labelCoefficients;
    private JButton btnRen;
    private JButton btnUpdate;
    private JTextField newPolynormial;
    private String fractionLegend = "Rational fraction";

    public OHLC_Fraction1() {
        setBackground(Color.WHITE);
        getContentPane().setLayout(new BorderLayout(0, 0));

        // labels to dispaly the expression and coefficients
        labelExpression = new JLabel("Expression: ");
        labelCoefficients = new JLabel("Coefficients: ");

        // create the initial dataset to populate the initial plot
        dataset = createDataset();

        jfreechart = ChartFactory.createXYLineChart(
                "Polynormial graph with fraction",
                "X value",
                "Y value",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        chartPanel = new ChartPanel(jfreechart);
        chartPanel.setMouseZoomable(true);

        panel = new JPanel(new BorderLayout());
        panel.add(chartPanel, BorderLayout.CENTER);
        getContentPane().add(panel);

        jfreechart.setBackgroundPaint(Color.WHITE);

        ValueMarker marker = new ValueMarker(1.3);  // position is the value on the axis
        marker.setPaint(Color.black);
        XYPlot plot = (XYPlot) jfreechart.getPlot();
        plot.addDomainMarker(marker);

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.RED);
        renderer.setSeriesPaint(1, Color.GREEN);
        renderer.setSeriesStroke(0, new BasicStroke(4.0f));
        renderer.setSeriesStroke(1, new BasicStroke(3.0f));
        plot.setRenderer(renderer);

        chartPanel.setBackground(Color.WHITE);
        panel.setBackground(Color.WHITE);

        btnRen = new JButton("rendererNUpdate");
        panel.add(btnRen, BorderLayout.NORTH);
        btnRen.addActionListener(this);

        btnUpdate = new JButton("Update graph");
        btnUpdate.setPreferredSize( new Dimension(40, 20) );

        // a panel to organize the expression, the coefficients and the button for update
        JPanel expressionPanel = new JPanel();
        expressionPanel.setLayout(new GridLayout(4, 1));

        newPolynormial = new JTextField("3,2");
        //12.9, -3.4, 2.1

        expressionPanel.add(labelExpression);
        expressionPanel.add(labelCoefficients);
        expressionPanel.add(newPolynormial);
        expressionPanel.add(btnUpdate);
        btnUpdate.addActionListener(this);

        // include this new expressionPanel in the overal panel
        panel.add(expressionPanel, BorderLayout.SOUTH);

        this.setVisible(true);
        this.setSize(600, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private XYDataset createDataset() {

        final XYSeries fraction = new XYSeries(fractionLegend);
        final int iStart = -30;
        final int iEnd = 30;

        final RealDistribution rng = new UniformRealDistribution(-100, 100);
        rng.reseedRandomGenerator(64925784252L);

        prepareFractionPlot(1, 2, fraction);

        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(fraction);

        return dataset;
    }

    @SuppressWarnings("deprecation")
    private JFreeChart createChart(final OHLCDataset dataset) {
        CandleStickRenderer = new CandlestickRenderer();
        CandleStickRenderer.setSeriesStroke(0, new BasicStroke(1.0f,
                BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        CandleStickRenderer.setSeriesPaint(0, Color.black);

        LineRenderer = new XYLineAndShapeRenderer(true, false);
        LineRenderer.setSeriesPaint(0, Color.BLACK);
        LineRenderer.setSeriesPaint(1, Color.BLUE);
        LineRenderer.setSeriesPaint(2, Color.RED);

        OHLCRenderer = new HighLowRenderer();
        OHLCRenderer.setSeriesStroke(0, new BasicStroke(1.0f,
                BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        OHLCRenderer.setSeriesPaint(0, Color.black);

        domainAxis = new DateAxis();
        domainAxis.setAutoRange(true);
        domainAxis.setTickLabelsVisible(true);
        domainAxis.setAutoTickUnitSelection(true);

        rangeAxis = new NumberAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createStandardTickUnits());
        rangeAxis.setAutoRange(true);

        plot = new XYPlot(dataset, domainAxis, rangeAxis, LineRenderer);
        plot.setBackgroundPaint(Color.LIGHT_GRAY);
        plot.setDomainGridlinePaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.WHITE);
        plot.setDomainGridlinesVisible(true);
        plot.setRangeGridlinesVisible(true);

        jfreechart = new JFreeChart("Stock Exchange price (IBM)", new Font(
                "SansSerif", Font.BOLD, 24), plot, false);
        return jfreechart;
    }

    public static void main(String[] args) {
        new OHLC_Fraction1();
    }


    @Override
    public void actionPerformed(ActionEvent ae) {
        XYPlot xyplot = (XYPlot) jfreechart.getPlot();
        if (ae.getSource() == btnUpdate) {
            // this does not seem to remove the old chart
            chartPanel.removeAll();
            chartPanel.revalidate();
            chartPanel.repaint();


            System.out.println("value of the text field: " + newPolynormial.getText());
            String newData = newPolynormial.getText().trim();
            String[] newAB = newData.split(",");

            double newA = Double.parseDouble(newAB[0]);
            double newB = Double.parseDouble(newAB[1]);

            XYDataset collec = createNewDataset(newA, newB);
            jfreechart.getXYPlot().setDataset(collec);


        } else if (ae.getSource() == btnRen) {
            xyplot.setRenderer(CandleStickRenderer);
            try {
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    static int a = 143;

    @SuppressWarnings("deprecation")
    private XYSeriesCollection createNewDataset(double newA, double newB) {
        final XYSeries fraction = new XYSeries(fractionLegend);

        final RealDistribution rng = new UniformRealDistribution(-100, 100);
        rng.reseedRandomGenerator(64925784252L);

        final PolynomialFunction f = prepareFractionPlot(newA, newB, fraction);

        UnivariateFunction derv = f.derivative();
        final XYSeries polyDerivative = new XYSeries("First Derivative");
        for (int j = -7; j < 5; j++) {
            final double randValue2 = j;
            final double xValue2 = randValue2;
            final double yValue2 = derv.value(xValue2);
            polyDerivative.add(xValue2, yValue2);
        }

        final XYSeriesCollection dataset = new XYSeriesCollection();

        dataset.addSeries(fraction);
        dataset.addSeries(polyDerivative);
        return dataset;
    }

    private PolynomialFunction prepareFractionPlot(double newA, double newB, XYSeries fraction) {
        final double[] coeff = {newA, newB}; // (x + 5)* (x-2) / (x+5)

        System.out.println("Fraction expression: " + "(x+" + newA + ")*(x-" + newB + ")/(x+" + newA + ")");
        labelExpression.setText("Expression: " + "(x+a)*(x-b)/(x+a)");
        labelCoefficients.setText("Coefficients: " + "a=" + newA + " and b=" + newB);

        final PolynomialFunction f = new PolynomialFunction(coeff);

        for (int i = -7; i < 5; i++) {
            final double randValue = i;
            double xValue = (randValue + newA) * (randValue - newB);
            double yValue = (randValue + newA);
            try {
                final Fraction f2 = new Fraction((int) xValue, (int) yValue);
                fraction.add(xValue, f2.getFractionDecimal());
            } catch (Exception e) {
                System.out.println(e.getMessage());
                fraction.add(xValue, Double.NaN);
            }
        }
        return f;
    }
}
