import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.function.Cos;
import org.apache.commons.math3.analysis.function.Sin;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.chart.renderer.xy.HighLowRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.OHLCDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class Part_3_Cos extends JFrame implements ActionListener {
    private XYDataset dataset;
    private CandlestickRenderer CandleStickRenderer;
    private DateAxis domainAxis;
    private NumberAxis rangeAxis;
    public static XYPlot plot;
    private JFreeChart jfreechart;
    public ChartPanel chartPanel;
    private JPanel panel;
    private XYLineAndShapeRenderer LineRenderer;
    private HighLowRenderer hlRemderer;
    private JLabel labelExpression;
    private JLabel labelCoefficients;
    private JButton btnRen;
    private JButton btnUpdate;
    private JTextField newPolynormial;
    private String fractionLegend = "Rational expression";

    // ranges for the x axis
    final int startX = -100;
    final int endX = 100;

    public Part_3_Cos() {
        setBackground(Color.WHITE);
        getContentPane().setLayout(new BorderLayout(0, 0));

        // labels to dispaly the expression and coefficients
        labelExpression = new JLabel("Expression: ");
        labelCoefficients = new JLabel("Coefficients: ");

        // create the initial dataset to populate the initial plot
        dataset = createDataset();

        jfreechart = ChartFactory.createXYLineChart(
                "Function expression",
                "a value",
                "b value",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        chartPanel = new ChartPanel(jfreechart);
        chartPanel.setMouseZoomable(true);

        panel = new JPanel(new BorderLayout());
        panel.add(chartPanel, BorderLayout.CENTER);
        getContentPane().add(panel);

        jfreechart.setBackgroundPaint(Color.WHITE);
        XYPlot plot = (XYPlot) jfreechart.getPlot();

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.BLUE);
        renderer.setSeriesPaint(1, Color.GREEN);
        renderer.setSeriesStroke(0, new BasicStroke(1.0f));
        renderer.setSeriesStroke(1, new BasicStroke(1.0f));
        plot.setRenderer(renderer);

        chartPanel.setBackground(Color.WHITE);
        panel.setBackground(Color.WHITE);

        btnRen = new JButton("rendererNUpdate");
        panel.add(btnRen, BorderLayout.NORTH);
        btnRen.addActionListener(this);

        btnUpdate = new JButton("Update graph");
        btnUpdate.setPreferredSize(new Dimension(40, 20));

        // a panel to organize the expression, the coefficients and the button for update
        JPanel expressionPanel = new JPanel();
        expressionPanel.setLayout(new GridLayout(4, 1));

        newPolynormial = new JTextField("3,2");
        expressionPanel.add(labelExpression);
        expressionPanel.add(labelCoefficients);
        expressionPanel.add(newPolynormial);
        expressionPanel.add(btnUpdate);
        btnUpdate.addActionListener(this);

        // include this new expressionPanel in the overal panel
        panel.add(expressionPanel, BorderLayout.SOUTH);

        this.setVisible(true);
        this.setSize(600, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private XYDataset createDataset() {

        final XYSeries _xySeries = new XYSeries(fractionLegend);
        preparePlot(_xySeries);

        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(_xySeries);

        return dataset;
    }

    @SuppressWarnings("deprecation")
    private JFreeChart createChart(final OHLCDataset dataset) {
        CandleStickRenderer = new CandlestickRenderer();
        CandleStickRenderer.setSeriesStroke(0, new BasicStroke(1.0f,
                BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        CandleStickRenderer.setSeriesPaint(0, Color.black);

        LineRenderer = new XYLineAndShapeRenderer(true, false);
        LineRenderer.setSeriesPaint(0, Color.BLACK);
        LineRenderer.setSeriesPaint(1, Color.BLUE);
        LineRenderer.setSeriesPaint(2, Color.RED);

        hlRemderer = new HighLowRenderer();
        hlRemderer.setSeriesStroke(0, new BasicStroke(1.0f,
                BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        hlRemderer.setSeriesPaint(0, Color.black);

        domainAxis = new DateAxis();
        domainAxis.setAutoRange(true);
        domainAxis.setTickLabelsVisible(true);
        domainAxis.setAutoTickUnitSelection(true);

        rangeAxis = new NumberAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createStandardTickUnits());
        rangeAxis.setAutoRange(true);

        plot = new XYPlot(dataset, domainAxis, rangeAxis, LineRenderer);
        plot.setBackgroundPaint(Color.LIGHT_GRAY);
        plot.setDomainGridlinePaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.WHITE);
        plot.setDomainGridlinesVisible(true);
        plot.setRangeGridlinesVisible(true);

        jfreechart = new JFreeChart("Cos Expression", new Font(
                "SansSerif", Font.BOLD, 24), plot, false);
        return jfreechart;
    }

    public static void main(String[] args) {
        new Part_3_Cos();
    }


    /**
     * Create an array of double numbers representing the range of the xvalues for the graph
     * @return
     */
    public ArrayList<Double> getXRanges() {
        ArrayList<Double> xValueList = new ArrayList<>();
        for (int i = startX + 1; i < endX; i++) {
            double xValue = (double) i / 10;
            xValueList.add(xValue);
        }
        return xValueList;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        XYPlot xyplot = (XYPlot) jfreechart.getPlot();
        if (ae.getSource() == btnUpdate) {
            String newData = newPolynormial.getText().trim();
            String[] newAB = newData.split(",");

            double newA = Double.parseDouble(newAB[0]);
            double newB = Double.parseDouble(newAB[1]);

            XYDataset collec = createNewDataset(newA, newB);
            jfreechart.getXYPlot().setDataset(collec);

        } else if (ae.getSource() == btnRen) {
            xyplot.setRenderer(CandleStickRenderer);
            try {
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @SuppressWarnings("deprecation")
    private XYSeriesCollection createNewDataset(double newA, double newB) {
        final XYSeries _xySeries = new XYSeries(fractionLegend);

        preparePlot(_xySeries);

        final Cos _logFunction = new Cos();
        UnivariateFunction derv = _logFunction.derivative();

        //todo: how to calculate the second derivative for a function?

        final XYSeries derivative1 = new XYSeries("First Derivative");
        //for (int j = startX + 1; j < endX; j++) {
        for (double xDev1 : getXRanges()) {
            final double yDev1 = (double) derv.value(xDev1);
            if (yDev1 != Double.NEGATIVE_INFINITY && yDev1 != Double.POSITIVE_INFINITY && yDev1 != Double.NaN) {
                //System.out.println("==> Adding xDev1=" + xDev1 + " and yDev1=" + yDev1);
                derivative1.add(xDev1, yDev1);
            } else {
                System.out.println("==> ignore xDev1=" + xDev1 + " and yDev1=" + yDev1);
            }
        }

        final XYSeriesCollection dataset = new XYSeriesCollection();

        dataset.addSeries(_xySeries);
        dataset.addSeries(derivative1);

        double yMax = _xySeries.getMaxY();
        double yMin = _xySeries.getMinY();

        plot = (XYPlot) jfreechart.getPlot();
        ValueMarker maxMarker = new ValueMarker(yMax);  // position is the value on the axis
        maxMarker.setPaint(Color.yellow);
        maxMarker.setLabel("Max Bound");
        plot.addRangeMarker(maxMarker);

        ValueMarker minMarker = new ValueMarker(yMin);  // position is the value on the axis
        minMarker.setPaint(Color.RED);
        minMarker.setLabel("Min Bound");
        plot.addRangeMarker(minMarker);

        //todo:
//        Use various colored lines and markers to find points of relative max/min --- 5 points
//        Use various colored lines and markers to find points of inflection --- 4 points
        return dataset;
    }

    private void preparePlot(XYSeries _xySeries) {

        final Cos _function = new Cos();
        UnivariateFunction derv = _function.derivative();

        double previousDevValue = (double) derv.value((double) startX / 10);
        System.out.println("previousDevValue=" + previousDevValue);

        for (double xValue : getXRanges()) {
            // could use a random double number, but let's just use the follow loop here
            double yValue = (double) _function.value(xValue);
            //System.out.println("-> Adding x=" + xValue + " and y=" + yValue);

            if (jfreechart != null) {
                double nextDevValue = (double) derv.value((double) xValue / 10);
                //System.out.println("-> nextDevValue=" + nextDevValue);

                if (nextDevValue * previousDevValue <= 0) {
                    System.out.println("Found an Inflection Point at xValue=" + xValue);
                    // let's add a vertical marker here
                    plot = (XYPlot) jfreechart.getPlot();
                    ValueMarker ipMarker = new ValueMarker(xValue);  // position is the value on the axis
                    ipMarker.setPaint(Color.red);
                    ipMarker.setLabel("IP"); //Inflection Point
                    plot.addDomainMarker(ipMarker);
                    previousDevValue = nextDevValue;
                }
            }

            try {
                if (yValue != Double.NEGATIVE_INFINITY && yValue != Double.POSITIVE_INFINITY) {
                    _xySeries.add(xValue, yValue);
                } else {
                    System.out.println("Ignore yValue=" + yValue);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                _xySeries.add(xValue, Double.NaN);
            }
        }

        System.out.println("=========================");
    }
}
